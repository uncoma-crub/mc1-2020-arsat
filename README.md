# Métodos computacionales para ingeniería 1 - UNComa (2020)

El siguiente repositorio contiene el trabajo elaborado por los estudiantes de las carreras de ingeniería de la UNComa durante el cursado de la materia Métodos computacionales para ingeniería 1.

# Segerencias en el modo de trabajo:

1) Cada unos de los alumnos del grupo debe realizar una revisón de la bibliografía sugerida y buscar más información acerca de los puntos que se piden desarrollar.

2) Distribuirse los puntos a discutir entre los alumnos a fin de abarcar la mayoría de los puntos.

3) Una vez asigandos los temas, se deberá elegir un usuario que sea el administrador del proyecto (puede ser cualquiera) y que haga lo que se llama un FORK del proyecto base

4) para que el grupo empieze a contribuir el resto del grupo deberá hacer un FORK del usuario administrador para empezar a contribuir.

5) Cómo se contribuye? Todos los usuarios (incluido el administrador del proyecto) deberá abrir una nueva rama asignándole un nombre característico sobre el tema del texto que vaya a  desarrollar.

6) Cuando los usuarios consideren terminado su trabajo deberán realizar lo que se conoce como MERGE REQUEST sobre el repositorio del usuario administrador.

7) Los alumnos del proyecto iran iterando con este procedimiento hasta alcanzar una version final de su trabajo.

8) Una vez que se tenga la version final se hará un MERGE REQUEST al master principal de la materia, donde el grupo presentará su informe final.

NOTAS: 
* Los alumnos aprenderán a utilizar git en forma grupal creando ramas y solicitando merges
* Los alumnos aprenderán a resolver conflictos en git (muy común en programación) de versiones del archivo que quieren elaborar 
* Tener mucho cuidado en el uso de ramas y cambio de ramas porque la información sigue estando en el proyecto. Se sugiere haber realizado la parte de Manejo de ramas con git
 del TP2 y eventualmente realizar backup en algun archivo de texto externo al proyecto hasta que se tenga un manejo confidente del sistema de control de versiones.

